import styled from 'styled-components';

const NavigationItemMainText = styled.div`
  overflow-x: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

NavigationItemMainText.displayName = 'NavigationItemMainText';
export default NavigationItemMainText;
