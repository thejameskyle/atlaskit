import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';
import styled from 'styled-components';

const ContainerLogo = styled.div`
  height: ${akGridSizeUnitless * 3}px;
  margin: ${akGridSizeUnitless * 3.5}px 0 ${akGridSizeUnitless * 3.5}px ${akGridSizeUnitless * 3};
`;

ContainerLogo.displayName = 'ContainerLogo';
export default ContainerLogo;
