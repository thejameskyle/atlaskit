import styled from 'styled-components';
import {
    akColorN800,
 } from '@atlaskit/util-shared-styles';

const SearchBox = styled.div`
  display: flex;
  color: ${akColorN800};
`;

SearchBox.displayName = 'SearchBox';
export default SearchBox;
