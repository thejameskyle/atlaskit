import React from 'react';
import ReactDOM from 'react-dom';

import App from './containers/App';

require('@atlaskit/css-reset');

ReactDOM.render(
  <App />,
  document.getElementById('app')
);
