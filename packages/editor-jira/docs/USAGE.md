# @NAME@

This component provides a WYSIWYG editor for JIRA. It's based on Atlassian Editor technology, but includes a JIRA specific
schema and user experience.

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).


## Installation

```sh
npm install @NAME@
```
